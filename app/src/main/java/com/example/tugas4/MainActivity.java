package com.example.tugas4;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import android.content.Intent;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        EditText angka_pertama = findViewById(R.id.angka_pertama);
        EditText angka_kedua = findViewById(R.id.angka_kedua);
        Button tambah = findViewById(R.id.btnTambah);
        Button kurang = findViewById(R.id.btnKurang);
        Button kali = findViewById(R.id.btnKali);
        Button bagi = findViewById(R.id.btnBagi);
        Button bersihkan = findViewById(R.id.btnClear);

        tambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if((angka_pertama.getText().length() > 0) && (angka_kedua.getText().length()>0)){
                    double angka1 = Double.parseDouble(angka_pertama.getText().toString());
                    double angka2 = Double.parseDouble(angka_kedua.getText().toString());
                    double result = angka1 + angka2;

                    String operatorString = "+";

                    String resultString = Double.toString(result);
                    String angka1String = Double.toString(angka1);
                    String angka2String = Double.toString(angka2);

                    Intent intent_mainactivity2 = new Intent(MainActivity.this, HalamanHasil.class);

                    intent_mainactivity2.putExtra("angka1String", angka1String);
                    intent_mainactivity2.putExtra("operatorString", operatorString);
                    intent_mainactivity2.putExtra("angka2String", angka2String);
                    intent_mainactivity2.putExtra("resultString", resultString);

                    startActivity(intent_mainactivity2);

                } else {
                    Toast toast = Toast.makeText(MainActivity.this, "Mohon masukkan angka pertama dan kedua.", Toast.LENGTH_LONG);
                    toast.show();
                }
            }
        });

        kurang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if((angka_pertama.getText().length() > 0) && (angka_kedua.getText().length()>0)){
                    double angka1 = Double.parseDouble(angka_pertama.getText().toString());
                    double angka2 = Double.parseDouble(angka_kedua.getText().toString());
                    double result = angka1 - angka2;

                    String operatorString = "-";

                    String resultString = Double.toString(result);
                    String angka1String = Double.toString(angka1);
                    String angka2String = Double.toString(angka2);

                    Intent intent_mainactivity2 = new Intent(MainActivity.this, HalamanHasil.class);

                    intent_mainactivity2.putExtra("angka1String", angka1String);
                    intent_mainactivity2.putExtra("operatorString", operatorString);
                    intent_mainactivity2.putExtra("angka2String", angka2String);
                    intent_mainactivity2.putExtra("resultString", resultString);

                    startActivity(intent_mainactivity2);

                } else {
                    Toast toast = Toast.makeText(MainActivity.this, "Mohon masukkan angka pertama dan kedua.", Toast.LENGTH_LONG);
                    toast.show();
                }
            }
        });

        kali.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if((angka_pertama.getText().length() > 0) && (angka_kedua.getText().length()>0)){
                    double angka1 = Double.parseDouble(angka_pertama.getText().toString());
                    double angka2 = Double.parseDouble(angka_kedua.getText().toString());
                    double result = angka1 * angka2;

                    String operatorString = "X";

                    String resultString = Double.toString(result);
                    String angka1String = Double.toString(angka1);
                    String angka2String = Double.toString(angka2);

                    Intent intent_mainactivity2 = new Intent(MainActivity.this, HalamanHasil.class);

                    intent_mainactivity2.putExtra("angka1String", angka1String);
                    intent_mainactivity2.putExtra("operatorString", operatorString);
                    intent_mainactivity2.putExtra("angka2String", angka2String);
                    intent_mainactivity2.putExtra("resultString", resultString);

                    startActivity(intent_mainactivity2);

                } else {
                    Toast toast = Toast.makeText(MainActivity.this, "Mohon masukkan angka pertama dan kedua.", Toast.LENGTH_LONG);
                    toast.show();
                }
            }
        });

        bagi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if((angka_pertama.getText().length() > 0) && (angka_kedua.getText().length()>0)){
                    double angka1 = Double.parseDouble(angka_pertama.getText().toString());
                    double angka2 = Double.parseDouble(angka_kedua.getText().toString());
                    double result = angka1 / angka2;

                    String operatorString = "/";

                    String resultString = Double.toString(result);
                    String angka1String = Double.toString(angka1);
                    String angka2String = Double.toString(angka2);

                    Intent intent_mainactivity2 = new Intent(MainActivity.this, HalamanHasil.class);

                    intent_mainactivity2.putExtra("angka1String", angka1String);
                    intent_mainactivity2.putExtra("operatorString", operatorString);
                    intent_mainactivity2.putExtra("angka2String", angka2String);
                    intent_mainactivity2.putExtra("resultString", resultString);

                    startActivity(intent_mainactivity2);

                } else {
                    Toast toast = Toast.makeText(MainActivity.this, "Mohon masukkan angka pertama dan kedua.", Toast.LENGTH_LONG);
                    toast.show();
                }
            }
        });

        bersihkan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                angka_pertama.getText().clear();
                angka_kedua.getText().clear();
            }
        });
    }
}