package com.example.tugas4;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import android.widget.TextView;

public class HalamanHasil extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.halaman_hasil);

        Bundle bundle_extra = getIntent().getExtras();

        String angka1_kiriman = bundle_extra.getString("angka1String");
        String operator_kiriman = bundle_extra.getString("operatorString");
        String angka2_kiriman = bundle_extra.getString("angka2String");
        String nilai_kiriman = bundle_extra.getString("resultString");

        TextView tvAngka1HalamanKedua = (TextView) findViewById(R.id.tvAngka1);
        tvAngka1HalamanKedua.setText(angka1_kiriman);

        TextView tvOperatorHalamanKedua = (TextView) findViewById(R.id.tvOperator);
        tvOperatorHalamanKedua.setText(operator_kiriman);

        TextView tvAngka2HalamanKedua = (TextView) findViewById(R.id.tvAngka2);
        tvAngka2HalamanKedua.setText(angka2_kiriman);

        TextView tvNilaiHalamanKedua = (TextView) findViewById(R.id.tvNilai);
        tvNilaiHalamanKedua.setText(nilai_kiriman);
    }
}